== INSTALLATION ====================================

Projekt importieren
Glassfish 4.1 Full Platform runterladen (Bei uns hat eine Funktion mit dem in Netbeans integrierten nicht funktioniert), und entpacken (Netbeans 8.0.1 benötigt, sonst wird der Server nicht erkannt)
Den Glassfish-Standalone-Server bei Netbeans hinzufügen
Das Deploy-Ziel des Projektes auf den neuen Server einstellen
Eine Datenbank für die App-Daten erstellen (Name "zuluvoting", Username und Password "APP")
Eine Datenbank für einen Timer erstellen (Name "timerDB", Username und Password "APP")
In der Timer-Datenbank eine Tabelle erstellen, mit "GLASSFISH_HOME\glassfish\lib\install\databases\ejbtimer_derby.sql" (Execute command)
Für jede Datenbank einen Connectionpool erstellen. Wir haben dafür den "New File/Glassfish/JDBC Resource"-Dialog in Netbeans benutzt. Die JNDI-Namen der Pools sind "jdbc/zuluvoting" und "jdbc/timerDB" und Object Type "system-all". Die Namen der Connectionpools sind egal. Es müssen jeweils die richtigen Datenbanken ausgewählt werden. Bei dem Pool für die Timer muss die "XA (Global Transaction)"-Checkbox aktiviert werden.
In "GLASSFISH_HOME/glassfish/domains/domain1/config" login.conf durch login.conf in diesem Ordner ersetzen, oder diesen Code: http://pastebin.com/MFsm0TSh anfügen
1.2 JAR von "http://flexiblejdbcrealm.wamblee.org/site/download.html" runterladen und in GLASSFISH_HOME/glassfish/glassfish/lib schieben
Glassfish-Adminkonsole starten
Unter "Configurations/server-config/security" Default Principal To Role Mapping enablen
Unter "Configurations/server-config/security/realms" neues Realm erstellen:
	Name = "FlexibleJdbcRealm"
	Class name zweites feld = "org.wamblee.glassfish.auth.FlexibleJdbcRealm"
	7 neue properties:
		"datasource.jndi" = "jdbc/zuluvoting"
		"sql.groups" = "SELECT USERGROUP FROM ORGANISER WHERE USERNAME = ?" // Unter Umständen muss hier erst ein "*" eintragen und später bearbeiten
		"sql.password" = "SELECT PASSWORD FROM ORGANISER WHERE USERNAME = ?" // Unter Umständen muss hier erst ein "*" eintragen und später bearbeiten
		"password.encoding" = "HEX:40"
		"password.digest" = "SHA"
		"jaas.context" = "FlexibleJdbcRealm"
		"charset" = "UTF-8"
Unter "Configurations/server-config/EJB Container/EJB Timer Service" die Timer Datasource auf "jdbc/timerDB" stellen

Um einen Admin zu erstellen, unter "/zuluvoting/test" auf "Create new test data." (1-mal) clicken. Der username ist "admin" und das Passwort "admin"


== REQUIREMENTS =====================================

1. Polls: Alle erfüllt
2. Poll States: Alle erfüllt
3. Organisers: Alle erfüllt außer 3.2.1, 3.5 und 3.5.1
4. Administrators: Alle erfüllt
5. Participants: Alle erfüllt außer 5.4 (Mail-Inhalt wird nur auf Konsole ausgegeben)
6. Participant lists: Keine erfüllt außer 6.1
7. Tokens: Alle erfüllt
8. Anonymity: Alle erfüllt, solange man nicht in die Datenbank guckt
9. Participation tracking: Nur 9.1 erfüllt
10. Submitting a vote: Alle erfüllt außer 10.10
11. Abstaining from voting: Alle erfüllt, für 11.2 stehen alle Items standartmäßig auf abstain
12. Types of items: Alle erfüllt außer 12.6 bis 12.8
13. Results: Alle erfüllt
14. User Interface: Alle erfüllt, kleinere Bildschirme werden nur eingeschränkt unterstützt
15. Security, encrypted communication: 15.1 erfüllt, 15.2 alle außer Poll-Teilnahme, 15.3 nicht erfüllt
16. Internationalization: Nicht erfüllt, das ganze System ist englisch
17. Browser Support: Alle erfüllt, getestet wurden Chrome und Firefox


== BESONDERHEITEN/ABWEICHUNGEN ======================

Tokens haben ein Feld valid.
Options speichern in einem Feld count wie oft für sie gevoted wurde.
Tokens sind immer mit Participants verlinkt, in der Datenbank, das ist aber nur von außerhalb merklich wenn participation tracking enabled ist.
Ansonsten weicht unser Datenmodell nicht von dem vorgegebenen ab.
Wir haben versucht Mails zu verschicken, der Code dafür ist da, wir konnten sogar tatsächlich testweise Mails verschicken, aber in Kombination mit den Timern hat es nicht geklappt.
Wir haben versucht M_OF_N-Fragen zu implementieren, aber wir sind auf einen Fehler gestoßen, den wir nicht lösen konnten. Auf unsere Frage hier: http://stackoverflow.com/q/26211038/4112204 haben wir bis zu Abgabe keine Antwort erhalten.
Wir haben ein Logo.
Unsere Projekt-Struktur ist anders als in der Vorlesung vergeschlagen, in Anlehnung an ein Buch mit dem wir gearbeiet haben. Persistenz-, Business/Application-, und Presentation-Schichten sind aber weitestgehend getrennt.