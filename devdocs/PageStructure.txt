																						Access?
Path						Description													Admin		Organiser		Participant

/lists						// Table containing all participant-lists					Yes			Owned lists		No
/lists/778899/participants	// Edit a list												No			Owned lists		No
/organisers					// Table containing all organisers							Yes			No				No
/polls						// Table containing all polls								Yes			Owned polls		No
/polls/001122				// Not used. 001122 represents the poll-id
/polls/001122/questions		// Edit the questions of a poll								No			If owned		No
/polls/001122/participants	// Edit and Manage the participants of a poll				No			If owned		No
/polls/001122/results		// See the results of a poll (when it's finished)			Yes			Yes				Yes
/polls/001122/take/aabbcc	// Take the survey. aabbcc represents the user-token		No			No				Yes (with token)