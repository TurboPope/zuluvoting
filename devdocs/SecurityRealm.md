*1.2 JAR von http://flexiblejdbcrealm.wamblee.org/site/download.html runterladen
*JAR in glassfish/glassfish/lib schieben
*glassfish/glassfish/domains/domain1/config/login.conf ersetzen durch login.conf in diesem Ordner
*Adminkonsole starten
*Configurations/server-config/security, und da Default Principal To Role Mapping enablen, save drücken
*Configurations/server-config/security/realms neues realm erstellen
..*name = "FlexibleJdbcRealm"
..*class name zweites feld = "org.wamblee.glassfish.auth.FlexibleJdbcRealm"
..*7 neue properties:
....*"datasource.jndi" = "jdbc/zuluvoting"
....*"sql.groups" = "SELECT USERGROUP FROM ORGANISER WHERE USERNAME = ?"
....*"sql.password" = "SELECT PASSWORD FROM ORGANISER WHERE USERNAME = ?"
....*"password.encoding" = "HEX:40"
....*"password.digest" = "SHA"
....*"jaas.context" = "FlexibleJdbcRealm"
....*"charset" = "UTF-8"
Rebuild, Depoly. ggf. Tabellen löschen