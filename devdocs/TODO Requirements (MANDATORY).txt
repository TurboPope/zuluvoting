TODO Requirements (MANDATORY)

1. Polls
	done

2. Poll states
	All Participants voted -> PollFinished! (done for partTracking enabled)

3. Organisers
	done

4. Admin (optional) done

5. Participants
	done
	information email senden(optional) - CONTENT NICHT!
	-> Mail muss enthalten: Title, Start- und Enddatum, Teilnehmerzahl, token done

6. Participant List
	done

7. Token
	random done
	unique (scope: system) 
	long enough to make it improbable to guess done

8. Anonymity
	ensure anonymity (reasonable effort)
	votes/tokens können nicht zurückverfolgt werden

9. Participation Tracking (optional)

10. Submitting a vote
	input field für participant token -> nach Verifizierung: items zeigen
	token invalidieren
	canceln muss möglich sein (token solange verwendbar bis submit)

11. Abstain from voting
	done

12. Types of items
	done

13. Results
	done (aber noch auskommentiert)
	muss anzahl der votes anzeigen + enthaltungen (hopefully done)





