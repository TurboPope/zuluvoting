package de.zulu.zuluvoting.controller;

import de.zulu.zuluvoting.persistence.entity.Organiser;
import de.zulu.zuluvoting.persistence.entity.UserGroup;
import de.zulu.zuluvoting.services.OrganiserService;
import de.zulu.zuluvoting.utils.PasswordEncoder;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@SessionScoped
@Named
public class CreateUserController implements Serializable {

    @Inject
    OrganiserService organiserService;

    private String password;

    private Organiser organiser = new Organiser();

    public Organiser getOrganiser() {
        return organiser;
    }

    public void setOrganiser(Organiser organiser) {
        this.organiser = organiser;
    }

    public String createUser() {
        organiser.setPassword(PasswordEncoder.encodePassword(password));
        organiser.setUserGroup(UserGroup.USER);
        try {
            organiserService.createOrganiser(organiser);
        } catch (Exception e) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "User already exists!", null);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            organiser = new Organiser();
            password = null;
            return Pages.CREATE_USER;
        }
        FacesMessage facesMessage = new FacesMessage(
                FacesMessage.SEVERITY_INFO, "New user succesfully created!", null);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        organiser = new Organiser();
        password = null;
        return Pages.CREATE_USER;

    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
