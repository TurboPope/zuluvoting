package de.zulu.zuluvoting.controller;

import de.zulu.zuluvoting.persistence.entity.Organiser;
import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.services.OrganiserService;
import de.zulu.zuluvoting.services.PollService;
import de.zulu.zuluvoting.services.UserService;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@SessionScoped
@Named
public class OrganisersController implements Serializable {

    @Inject
    PollService pollService;

    @Inject
    OrganiserService organiserService;

    @Inject
    UserService userService;

    private Poll poll;

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public String doDeleteOrganiser(Organiser organiser) {
        if (userService.isAdministrator()) {
            organiserService.deleteOrganiser(organiser);
        } else {
            pollService.removeOrganiserFromPoll(poll, organiser);
        }
        return Pages.ORGANISER_LIST;
    }

    public List<Organiser> getAllOrganisers() {
        return organiserService.getAllOrgnanisers();
    }
}
