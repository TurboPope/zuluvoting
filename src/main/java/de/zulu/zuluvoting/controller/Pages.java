package de.zulu.zuluvoting.controller;

public class Pages {

    public static final String POLL_LIST = "pollList";
    public static final String PARTICIPANT_LIST = "participants";
    public static final String POLL_EDIT = "pollEdit";
    public static final String ORGANISER_LIST = "organisers";
    public static final String PREVIEW = "pollPreview";
    public static final String CREATE_USER = "createUser";
    public static final String INDEX = "index";
    public static final String VOTED = "voted";
}
