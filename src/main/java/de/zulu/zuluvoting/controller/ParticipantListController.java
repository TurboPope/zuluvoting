package de.zulu.zuluvoting.controller;

import de.zulu.zuluvoting.persistence.entity.Participant;
import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.persistence.entity.Token;
import de.zulu.zuluvoting.services.PollService;
import java.io.Serializable;
import java.util.UUID;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@SessionScoped
@Named
public class ParticipantListController implements Serializable {

    public enum Mode {
        EDIT,
        READONLY
    }

    private Mode mode;

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    @Inject
    PollService pollService;

    private Poll poll;

    private String newEmail;

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String doDeleteParticipant(Participant participant) {
        pollService.removeParticipantFromPoll(poll, participant);

        removeCorrespondingToken(participant);

        return Pages.PARTICIPANT_LIST;
    }

    public String doAddParticipant() {
        Participant participant = new Participant();
        participant.setEmail(newEmail);
        participant.setHasVoted(false);
        newEmail = null;

        if (poll.getParticipants().contains(participant)) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Participants have to be unique.", null);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            return Pages.PARTICIPANT_LIST;
        }
        poll.getParticipants().add(participant);

        createToken(participant);

        return Pages.PARTICIPANT_LIST;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public String doContinue() {
        if (poll.getParticipants().size() < 3) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "You have to specify at least 3 participants!", null);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            return Pages.PARTICIPANT_LIST;
        }
        poll = null;
        mode = null;
        return Pages.POLL_EDIT;
    }

    private void createToken(Participant p) {
        Token token = new Token();
        token.setParticipant(p);
        token.setPoll(poll);
        token.setValue(UUID.randomUUID().toString());
        poll.getTokens().add(token);
    }

    private void removeCorrespondingToken(Participant p) {
        Token tokenToRemove = null;
        for (Token t : poll.getTokens()) {
            if (t.getParticipant().equals(p)) {
                tokenToRemove = t;
                break;
            }
        }
        poll.getTokens().remove(tokenToRemove);
    }
}
