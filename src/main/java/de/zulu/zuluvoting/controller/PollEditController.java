package de.zulu.zuluvoting.controller;

import de.zulu.zuluvoting.persistence.entity.Item;
import de.zulu.zuluvoting.persistence.entity.ItemType;
import de.zulu.zuluvoting.persistence.entity.Option;
import de.zulu.zuluvoting.persistence.entity.Organiser;
import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.persistence.entity.PollState;
import de.zulu.zuluvoting.services.PollService;
import de.zulu.zuluvoting.services.UserService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@SessionScoped
@Named
public class PollEditController implements Serializable {
    //--(ggf neu erstellte) Poll wird von Übersichtsliste (bzw. deren Controller) übergeben

    @Inject
    ParticipantListController participantListController;
    @Inject
    OrganisersController organisersController;
    @Inject
    PollPreviewController previewController;

    @Inject
    PollService pollService;

    @Inject
    UserService userService;

    public enum Mode {
        EDIT,
        ADD
    };

    private Poll poll;
    private Mode mode;
    private Date extension = null;

    public Date getExtension() {
        return extension;
    }

    public void setExtension(Date extension) {
        this.extension = extension;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public String doCancel() {
        return Pages.POLL_LIST;
    }

    //Bearbeite Fragen
    public void doAddItem() {
        poll.getItems().add(new Item());

    }

    public String doEditParticipators() {
        participantListController.setPoll(poll);
        if (poll.getState() != PollState.PREPARED) {
            participantListController.setMode(ParticipantListController.Mode.READONLY);
        } else {
            participantListController.setMode(ParticipantListController.Mode.EDIT);
        }
        return Pages.PARTICIPANT_LIST;
    }

    public void setPollToEdit(Mode mode) {
        Poll poll = new Poll();
        poll.setState(PollState.PREPARED);
        Organiser currentOrganiser = userService.getOrganiser();
        poll.getOrganisers().add(currentOrganiser);
        setPollToEdit(mode, poll);
    }

    private void createErrorMessage(String content) {
        FacesMessage facesMessage = new FacesMessage(
                FacesMessage.SEVERITY_ERROR, content, null);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    private void createInfoMessage(String content) {
        FacesMessage facesMessage = new FacesMessage(
                FacesMessage.SEVERITY_INFO, content, null);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    private boolean checkItems() {
        HashSet<String> titles = new HashSet<String>();

        boolean ret = true;
        for (Item i : poll.getItems()) {
            if (i.getTitle() == null) {
                createErrorMessage("Items need to have a name.");
                ret = false;
            }
            if (titles.contains(i.getTitle())) {
                createErrorMessage("2 Items can't have the same name.");
                ret = false;
            } else {
                titles.add(i.getTitle());
            }
            if (i.getOptions().size() < 3) {
                //Check if less Options than 3 (abstention + 2 options)
                createErrorMessage("You need 2 options per item");
                ret = false;
            }
        }

        return ret;
    }

    private boolean checkConstraints() {
        boolean ret = true;

        if ((poll.getTitle() == null)) {
            createErrorMessage("Polls need a title.");
            ret = false;
        }
        if ((poll.getDescription() == null)) {
            createErrorMessage("Polls need a description.");
            ret = false;
        }

        if ((poll.getItems().size() < 1)) {
            //0 Items
            createErrorMessage("You need at least 1 item.");
            ret = false;
        }
        if (poll.getStart() == null || poll.getEnd() == null) {
            //Dates null
            createErrorMessage("You need to specify dates.");
            ret = false;
        } else if (poll.getStart().after(poll.getEnd())) {
            //Dates in wrong order
            createErrorMessage("Your start date can't be after your end date.");
            ret = false;
        } else if (poll.getStart().before(new Date())) {
            createErrorMessage("The start date has to be in the future.");
            ret = false;
        }

        if (poll.getParticipants().size() < 3) {
            createErrorMessage("You have to specify at least 3 participants.");
            ret = false;
        }
        if (checkItems() == false) {
            ret = false;
        }

        return ret;
    }

    public void doSave() {
        if (checkConstraints() == true) {
            //Save
            if (getMode() == Mode.ADD) {
                try {
                    pollService.addPoll(poll);
                } catch (Exception e) {
                    createErrorMessage("Poll name is already taken.");
                    e.printStackTrace();
                    return;
                }

                setMode(Mode.EDIT);
            } else {
                try {
                    pollService.updatePoll(poll);
                } catch (Exception e) {
                    createErrorMessage("Poll name is already taken.");
                    e.printStackTrace();
                    return;
                }
            }
            createInfoMessage("Poll saved");
        }
    }

    public void doDeleteItem(Item item) {
        poll.getItems().remove(item);
    }

    //Bearbeite Antworten
    public void doAddOption(Item item) {
        int position = poll.getItems().indexOf(item);
        poll.getItems().get(position).getOptions().add(new Option());
    }

    public void doDeleteOption(Item item, Option option) {
        int position = poll.getItems().indexOf(item);
        poll.getItems().get(position).getOptions().remove(option);
    }

    public String doGetPreview() {
        previewController.setPoll(poll);
        return Pages.PREVIEW;
    }

    public String doEditOrganisers() {
        organisersController.setPoll(poll);
        return Pages.ORGANISER_LIST;
    }

    public
            void setPollToEdit(Mode mode, Poll poll) {
        this.poll = poll;
        this.mode = mode;
    }

    public String getTitle() {
        if (getMode() == Mode.EDIT) {
            return "Edit Poll";
        }
        return "Create New Poll";
    }

    public String doStart() {
        poll.setState(PollState.STARTED);
        doSave();
        return Pages.POLL_LIST;
    }

    public void doExtend() {
        if (extension != null) {
            if (poll.getEnd().compareTo(extension) > 0) {
                createErrorMessage("You can only extend your voting period.");
            } else {
                poll.setEnd(extension);
                pollService.updatePoll(poll);
                extension = null;

            }
        } else {
            createErrorMessage("Please enter a date to extend to.");
        }
    }

    public List<Option> initOptions() {
        List<Option> options = new ArrayList<>();
        //Abstain on every item
        Option abstain = new Option();
        abstain.setShortName("Abstention");
        options.add(abstain);
        return options;
    }

    public void onItemTypeChange(Item item) {
        if (item.getType() == ItemType.YES_NO) {
            List options = initOptions();
            Option yes = new Option();
            yes.setShortName("Yes");
            Option no = new Option();
            no.setShortName("No");
            options.add(yes);
            options.add(no);
            item.setOptions(options);
        } else {
            item.setOptions(initOptions());
        }
    }

}
