package de.zulu.zuluvoting.controller;

import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.services.PollService;
import de.zulu.zuluvoting.services.UserService;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@SessionScoped
@Named
public class PollListController implements Serializable {

    @Inject
    OrganisersController organisersController;
    @Inject
    ParticipantListController participantListController;
    @Inject
    PollEditController pollEditController;

    @Inject
    PollService pollService;

    @Inject
    UserService userService;

    public List<Poll> viewAllPolls() {
        return pollService.getAllPolls();
    }

    public List<Poll> getPollsOwnedByOrganiser() {
        return pollService.getAllPollsOwnedByCurrentUser();
    }

    public String doViewParticipantList(Poll poll) {
        participantListController.setPoll(poll);
        participantListController.setMode(ParticipantListController.Mode.READONLY);
        return Pages.PARTICIPANT_LIST;
    }

    public String doViewOrganiserList(Poll poll) {
        organisersController.setPoll(poll);
        return Pages.ORGANISER_LIST;
    }

    public String doDeletePoll(Poll poll) {
        pollService.deletePoll(poll);
        return Pages.POLL_LIST;
    }

    public String doAddPoll() {
        pollEditController.setPollToEdit(PollEditController.Mode.ADD);
        return Pages.POLL_EDIT;
    }

    public String doEditPoll(Poll poll) {
        pollEditController.setPollToEdit(PollEditController.Mode.EDIT, poll);
        return Pages.POLL_EDIT;
    }

    public String getResultURL(Poll poll) {
        return "https://localhost:8181/zuluvoting/results?id=" + poll.getId();
    }

    public String getFormattedState(Poll poll) {
        String result = poll.getState().toString().substring(0, 1).toUpperCase() + poll.getState().toString().substring(1).toLowerCase();
        return result;
    }

    public String formatStart(Poll poll) {
        long now = System.currentTimeMillis();
        long start = poll.getStart().getTime();
        String formatted = formatPeriod(start, now);
        switch (poll.getState()) {
            case PREPARED:
                if (now < start) {
                    return "(In " + formatted + ")";
                } else {
                    return "(" + formatted + " ago)";
                }
            case STARTED:
                return "In " + formatted;
            case RUNNING:
                return formatted + " ago";
            case FINISHED:
                return formatted + " ago";
        }
        return null;
    }

    public String formatEnd(Poll poll) {
        long now = System.currentTimeMillis();
        long end = poll.getEnd().getTime();
        String formatted = formatPeriod(now, end);
        switch (poll.getState()) {
            case PREPARED:
                if (now < end) {
                    return "(In " + formatted + ")";
                } else {
                    return "(" + formatted + " ago)";
                }
            case STARTED:
                return "In " + formatted;
            case RUNNING:
                return "In " + formatted;
            case FINISHED:
                return formatted + " ago";
        }
        return null;
    }

    private static final long SECOND = 1000;
    private static final long MINUTE = SECOND * 60;
    private static final long HOUR = MINUTE * 60;
    private static final long DAY = HOUR * 24;

    private static String formatPeriod(long start, long end) {
        long diff = Math.abs(end - start);
        String formatted;
        if (diff < MINUTE) {
            long seconds = (diff / SECOND);
            formatted = seconds + " " + (seconds == 1 ? "second" : "seconds");
        } else if (diff < HOUR) {
            long minutes = (diff / MINUTE);
            formatted = minutes + " " + (minutes == 1 ? "minute" : "minutes");
        } else if (diff < DAY) {
            long hours = (diff / HOUR);
            formatted = hours + " " + (hours == 1 ? "hour" : "hours");
        } else {
            long days = (diff / DAY);
            formatted = days + " " + (days == 1 ? "day" : "days");
        }
        return "~" + formatted;
    }

    public String getVotesMessage(Poll poll) {
        if (poll.isParticipationTracking()) {
            return poll.getVoteCount() + "/" + poll.getParticipants().size();
        } else {
            return "?/" + poll.getParticipants().size();
        }
    }
}
