package de.zulu.zuluvoting.controller;

import de.zulu.zuluvoting.persistence.entity.Item;
import de.zulu.zuluvoting.persistence.entity.Option;
import de.zulu.zuluvoting.persistence.entity.Poll;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

@RequestScoped
@Named
public class PollPreviewController implements Serializable {

    private Poll poll;
    private final Map<Long, Option> selectedRadioOptions = new HashMap<>(); // ItemId -> Option[]
    private final Map<Long, List<SelectItem>> availableItemsWithOptions = new HashMap<>(); // ItemId -> SelectItem[]

    private final Map<Long, Item> temporaryItemMap = new HashMap<>();

    public Map<Long, Option> getSelectedRadioOptions() {
        return selectedRadioOptions;
    }

    public Map<Long, List<SelectItem>> getAvailableItemsWithOptions() {
        return availableItemsWithOptions;
    }

    public void setPoll(Poll p) {
        poll = p;
        init();
    }

    public Poll getPoll() {
        return poll;
    }

    public void init() {
        long tempId = 0;
        for (Item item : poll.getItems()) {
            List<SelectItem> optionsList = new ArrayList<>();
            for (int i = 0; i < item.getOptions().size(); i++) {
                Option option = item.getOptions().get(i);
                String description = "";
                if (option.getDescription() != null && !option.getDescription().equals("")) {
                    description = "\t// " + option.getDescription();
                }
                optionsList.add(new SelectItem(
                        option,
                        "<b>" + option.getShortName() + "</b>"
                        + description,
                        option.getDescription(),
                        false,
                        false
                )
                );
            }

            temporaryItemMap.put(tempId, item);
            selectedRadioOptions.put(tempId, item.getOptions().get(0));
            availableItemsWithOptions.put(tempId, optionsList);
            tempId++;
        }

        RequestContext.getCurrentInstance().update("pollForm");
    }

    public Item getItemFromListById(Long id) {

        return temporaryItemMap.get(id);
    }

    public List<Long> getItemIds() {
        return new ArrayList<>(availableItemsWithOptions.keySet());
    }

    public List<SelectItem> getSelectItemsByItemId(Long itemId) {
        return availableItemsWithOptions.get(itemId);
    }
    
    public String doContinue(){
        return Pages.POLL_EDIT;
    }

}
