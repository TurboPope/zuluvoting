package de.zulu.zuluvoting.controller;

import de.zulu.zuluvoting.persistence.entity.Item;
import de.zulu.zuluvoting.persistence.entity.Option;
import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.persistence.entity.PollState;
import de.zulu.zuluvoting.services.PollService;
import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import org.primefaces.model.chart.PieChartModel;

@RequestScoped
@ManagedBean
public class ResultController implements Serializable {

    @Inject
    PollService pollService;

    private Poll poll;
    private long pollid;
    private State state;
    private ArrayList<PieChartModel> pies;

    public ArrayList<PieChartModel> getPies() {
        return pies;
    }

    public void setPies(ArrayList<PieChartModel> pies) {
        this.pies = pies;
    }

    public enum State {

        RESULTS,
        LOWPARTICIPANTS,
        NOTFINISHED
    };

    public long getPollid() {
        return pollid;
    }

    public void setPollid(long pollid) {
        setPoll(pollService.findPollById(pollid));
        this.pollid = pollid;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {

        if (poll.getState() == PollState.FINISHED) {
            if (poll.getVoteCount() < 3) {
                state = State.LOWPARTICIPANTS;
            } else {
                state = State.RESULTS;
            }
        } else {
            state = State.NOTFINISHED;
        }
        this.poll = poll;
        pies = new ArrayList<>();
        createPies(poll);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void createPies(Poll poll) {
        for (Item i : poll.getItems()) {
            pies.add(createPieForItem(i));
        }
    }

    public PieChartModel createPieForItem(Item item) {
        PieChartModel pie = new PieChartModel();

        for (Option o : item.getOptions()) {
            pie.set(o.getShortName(), o.getCount());
        }
        pie.setLegendPosition("w");
        pie.setShowDataLabels(true);
        return pie;
    }

    public String doCreateParticipationString() {
        return poll.getVoteCount() + " of " + poll.getParticipants().size() + " participants have voted.";
    }

    public String percent(Option option) {
        if (option.getCount() > 0) {
            return (((float) option.getCount() * 100) / ((float) poll.getVoteCount())) + "%";
        } else {
            return "0.0%";
        }
    }
}
