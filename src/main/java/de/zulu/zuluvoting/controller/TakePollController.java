package de.zulu.zuluvoting.controller;

import de.zulu.zuluvoting.persistence.entity.Item;
import de.zulu.zuluvoting.persistence.entity.Option;
import de.zulu.zuluvoting.persistence.entity.Participant;
import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.persistence.entity.Token;
import de.zulu.zuluvoting.services.ParticipantService;
import de.zulu.zuluvoting.services.PollService;
import de.zulu.zuluvoting.services.TokenService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;

@ViewScoped
@ManagedBean
public class TakePollController implements Serializable {

    @Inject
    private PollService pollService;
    @Inject
    private TokenService tokenService;
    @Inject
    private ParticipantService participantService;
    private String rawToken;
    private Token token;
    private Poll poll;
    private final Map<Long, Option> selectedRadioOptions = new HashMap<>(); // ItemId -> Option[]
    private final Map<Long, List<SelectItem>> availableItemsWithOptions = new HashMap<>(); // ItemId -> SelectItem[]

    public Map<Long, Option> getSelectedRadioOptions() {
        return selectedRadioOptions;
    }

    public Map<Long, List<SelectItem>> getAvailableItemsWithOptions() {
        return availableItemsWithOptions;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public String getRawToken() {
        return rawToken;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setRawToken(String rawToken) {
        this.rawToken = rawToken;
        try {
            this.token = tokenService.findTokenByValue(this.rawToken);
        } catch (EJBException e) {
            this.token = null;
        }
        if (token != null) {
            this.poll = token.getPoll();
            for (Item item : poll.getItems()) {
                List<SelectItem> optionsList = new ArrayList<>();
                for (int i = 0; i < item.getOptions().size(); i++) {
                    Option option = item.getOptions().get(i);
                    String description = "";
                    if (option.getDescription() != null && !option.getDescription().equals("")) {
                        description = "\t// " + option.getDescription();
                    }
                    optionsList.add(new SelectItem(
                            option,
                            "<b>" + option.getShortName() + "</b>"
                            + description,
                            option.getDescription(),
                            false,
                            false
                    )
                    );
                }
                selectedRadioOptions.put(item.getId(), item.getOptions().get(0));
                availableItemsWithOptions.put(item.getId(), optionsList);
            }
        }
        RequestContext.getCurrentInstance().update("pollForm");
        RequestContext.getCurrentInstance().update("tokenForm");
    }

    public Item getItemFromListById(Long id) {
        for (Item item : poll.getItems()) {
            if (item.getId().equals(id)) {
                return item;
            }
        }
        return null;
    }

    public List<Long> getItemIds() {
        return new ArrayList<>(availableItemsWithOptions.keySet());
    }

    public List<SelectItem> getSelectItemsByItemId(Long itemId) {
        return availableItemsWithOptions.get(itemId);
    }

    public String submit() {
        if (token.isValid()) {
            for (Long itemId : selectedRadioOptions.keySet()) {
                Option selectedOption = selectedRadioOptions.get(itemId);
                if (selectedOption == null) {
                    continue;
                }
                for (Item item : poll.getItems()) {
                    if (itemId.equals(item.getId())) {
                        for (Option option : item.getOptions()) {
                            if (option.getId().equals(selectedOption.getId())) {
                                option.setCount(option.getCount() + 1);
                            }
                        }
                    }
                }
            }

            poll.setVoteCount(poll.getVoteCount() + 1);
            pollService.updatePoll(poll);
            token.setValid(false);
            Participant participant = this.token.getParticipant();
            participant.setHasVoted(true);
            participantService.updateParticipant(participant);
            tokenService.updateToken(token);
            this.token = null;
            this.rawToken = null;
            this.poll = null;
            return Pages.VOTED;
        } else {
            RequestContext.getCurrentInstance().update("pollForm");
            RequestContext.getCurrentInstance().update("tokenForm");
            return null;
        }
    }
}
