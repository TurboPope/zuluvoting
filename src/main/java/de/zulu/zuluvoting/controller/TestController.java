package de.zulu.zuluvoting.controller;

import de.zulu.zuluvoting.persistence.entity.Item;
import de.zulu.zuluvoting.persistence.entity.ItemType;
import de.zulu.zuluvoting.persistence.entity.Option;
import de.zulu.zuluvoting.persistence.entity.Organiser;
import de.zulu.zuluvoting.persistence.entity.Participant;
import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.persistence.entity.PollState;
import de.zulu.zuluvoting.persistence.entity.UserGroup;
import de.zulu.zuluvoting.services.MailService;
import de.zulu.zuluvoting.services.PollService;
import de.zulu.zuluvoting.services.TokenService;
import de.zulu.zuluvoting.utils.PasswordEncoder;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@Named
public class TestController {

    @Inject
    MailService mailService;

    @Inject
    TokenService tokenService;

    @Inject
    PollService pollService;

    public void testTokenService() {
        List<Poll> polls = pollService.getAllPolls();
        for (Poll p : polls) {
            tokenService.sendMessageToParticipants(p);
        }

    }

    public void sendTestMessage() {
        mailService.setSubject("Test email from Zuluvoting");
        mailService.setBody("This is a test message");
        mailService.setTo("bijak.dawid@gmail.com");
        try {
            mailService.send();
        } catch (Exception ex) {
            System.err.println("Error at sending e-mail");
        }
    }

    @PersistenceContext
    private EntityManager em;

    public void createTestData() {
        /*
        List<Poll> list = createPolls();
        for(Poll p : list)
            em.persist(p);
        
        Organiser testOrganiser = new Organiser();
        testOrganiser.setEmail("test@test.de");
        testOrganiser.setPassword("9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684");
        testOrganiser.setRealname("Hans");
        testOrganiser.setUsername("testOrganiser");
        testOrganiser.setUserGroup(UserGroup.USER);
        em.persist(testOrganiser);
         */
        Organiser admin = new Organiser();
        admin.setEmail("admin@zuluvoting.com");
        admin.setPassword(PasswordEncoder.encodePassword("admin"));
        admin.setRealname("ADMIN");
        admin.setUsername("admin");
        admin.setUserGroup(UserGroup.ADMIN);
        em.persist(admin);

    }

    public LinkedList<Participant> createParticipants(String name, int amount) {
        LinkedList<Participant> participants = new LinkedList<>();
        for (int i = 0; i < amount; i++) {
            Participant participant = new Participant();
            participant.setEmail(name + i + "@example.com");
            participant.setHasVoted(i % 2 == 0);
            participants.add(participant);
        }
        return participants;
    }

    public LinkedList<Organiser> createOrganiserss(String name, int amount) {
        LinkedList<Organiser> organisers = new LinkedList<>();
        for (int i = 0; i < amount; i++) {
            Organiser organiser = new Organiser();
            organiser.setEmail(name + i + "@example.com");
            organiser.setPassword("pass" + i);
            organiser.setRealname(name + i);
            organiser.setUsername("Nick" + i);
            //missing List of Polls (easier with SQL requests)
            organisers.add(organiser);
        }
        return organisers;
    }

    public List<Poll> createPolls() {
        //Poll 1
        Poll poll1 = new Poll();
        poll1.setTitle("Your Opinion on Mountains");
        poll1.setDescription("This poll seeks to come to a general consensus about mountains and various mountain-related topics. "
                + "You may participate in this poll even if you do not believe in mountains.");
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(2013, 2, 1, 10, 0, 0);
        poll1.setStart(cal.getTime());
        cal.set(2014, 2, 1, 5, 0, 0);
        poll1.setEnd(cal.getTime());
        poll1.setState(PollState.PREPARED);
        poll1.setParticipationTracking(true);
        poll1.setParticipants(createParticipants("Part_(Poll1)_", 10));
        poll1.setOrganisers(createOrganiserss("Organiser_(Poll1)_", 2));

        //Frage 1.1
        Item item11 = new Item();
        item11.setTitle("Do you believe in mountains?");
        item11.setType(ItemType.YES_NO);
        Option option111 = new Option();
        option111.setShortName("");
        option111.setDescription("");
        LinkedList<Option> options11 = new LinkedList<>();
        options11.add(option111);
        item11.setOptions(options11);

        //Frage 1.2
        Item item12 = new Item();
        item12.setTitle("Have you ever seen a mountain?");
        item12.setType(ItemType.YES_NO);
        Option option121 = new Option();
        option121.setShortName("");
        option121.setDescription("");
        LinkedList<Option> options12 = new LinkedList<>();
        options12.add(option121);
        item12.setOptions(options12);

        //Frage 1.3
        Item item13 = new Item();
        item13.setTitle("Which of the following words best describes your feelings towards mountains?");
        item13.setType(ItemType.ONE_OF_N);
        Option option131 = new Option();
        option131.setShortName("Hazelnut");
        option131.setDescription("");
        Option option132 = new Option();
        option132.setShortName("Cuttlefish");
        option132.setDescription("");
        Option option133 = new Option();
        option133.setShortName("Lurk");
        option133.setDescription("");
        Option option134 = new Option();
        option134.setShortName("Laser");
        option134.setDescription("");
        Option option135 = new Option();
        option135.setShortName("Falafel");
        option135.setDescription("");
        LinkedList<Option> options13 = new LinkedList<>();
        options13.add(option131);
        options13.add(option132);
        options13.add(option133);
        options13.add(option134);
        options13.add(option135);
        item13.setOptions(options13);

        //Baue Fragen Liste
        LinkedList<Item> items1 = new LinkedList<>();
        items1.add(item11);
        items1.add(item12);
        items1.add(item13);
        poll1.setItems(items1);

        //Poll 2
        Poll poll2 = new Poll();
        poll2.setTitle("Umfrage 2");
        poll2.setDescription("Ich bin Umfrage 2");
        cal.set(2014, 1, 1, 10, 0);
        poll2.setStart(cal.getTime());
        cal.set(2014, 11, 1, 9, 30);
        poll2.setEnd(cal.getTime());
        poll2.setState(PollState.PREPARED);
        poll2.setParticipationTracking(true);
        poll2.setParticipants(createParticipants("Part_(Poll2)_", 5));
        poll2.setOrganisers(createOrganiserss("Organiser_(Poll2)_", 3));

        //Frage 2.1
        Item item21 = new Item();
        item21.setTitle("Frage 1");
        item21.setType(ItemType.ONE_OF_N);
        //Antwort 2.1.1
        Option option211 = new Option();
        option211.setShortName("Antwort 1");
        option211.setDescription("Beschreibung von Antwort 1");

        //Antwortliste bauen
        List<Option> options21 = new ArrayList<>();
        options21.add(option211);
        item21.setOptions(options21);

        //Baue Fragen Liste
        List<Item> items2 = new ArrayList<>();
        items2.add(item21);
        poll2.setItems(items2);

        List<Poll> ret = new ArrayList<>();
        ret.add(poll1);
        ret.add(poll2);
        return ret;
    }
}
