package de.zulu.zuluvoting.data;

import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.services.PollService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@RequestScoped
@Named
public class PollListProducer {

    private List<Poll> polls;

    @Inject
    private PollService pollService;

    @PostConstruct
    public void init() {
        polls = pollService.getAllPolls();
    }

    public List<Poll> getPolls() {
        return polls;
    }

}
