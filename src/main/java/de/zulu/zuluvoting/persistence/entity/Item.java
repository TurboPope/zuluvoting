package de.zulu.zuluvoting.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

@Entity
public class Item implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @Enumerated(EnumType.STRING)
    private ItemType type;
    /**
     * ???
     */
    private int m;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @Size(min = 3) //at least 3, 1 abstention + 2 options
    private List<Option> options;

    public Item() {
        options = new ArrayList<>();
        //Abstain on every item
        Option abstain = new Option();
        abstain.setShortName("Abstention");
        options.add(abstain);

    }

    public Item(Long id, String title, ItemType type, List<Option> options) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.options = options;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "Item{"
                + "id=" + id
                + ", title='" + title + '\''
                + ", type=" + type
                + ", m=" + m
                + ", options=" + options
                + '}';
    }
}
