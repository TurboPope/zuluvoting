package de.zulu.zuluvoting.persistence.entity;

public enum ItemType {
    /**
     * The participant may only choose between yes and no.
     */
    YES_NO,
    /**
     * The participant may choose exactly one on n options.
     */
    ONE_OF_N,
    /**
     * The participant may choose an arbitrary number of options.
     */
    M_OF_N
}
