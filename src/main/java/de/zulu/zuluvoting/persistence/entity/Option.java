package de.zulu.zuluvoting.persistence.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Option_Table")
public class Option implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Option() {
    }

    public Option(Long id, String shortName) {
        this.id = id;
        this.shortName = shortName;
        this.count = 0;
    }

    private String shortName;
    private String description;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Option{"
                + "id=" + id
                + ", shortName='" + shortName + '\''
                + ", description='" + description + '\''
                + ", count=" + count
                + '}';
    }

}
