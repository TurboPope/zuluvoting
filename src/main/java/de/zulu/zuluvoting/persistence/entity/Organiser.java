package de.zulu.zuluvoting.persistence.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Pattern;

@NamedQueries({
    @NamedQuery(name = Organiser.findByUsername, query = "SELECT o FROM Organiser o WHERE o.username = :username"),
    @NamedQuery(name = Organiser.findAllOrganisers, query = "SELECT o FROM Organiser o WHERE o.username != 'admin'")
})
@Entity
public class Organiser implements Serializable {

    public static final String findByUsername = "Organiser.findByUsername";
    public static final String findAllOrganisers = "Organiser.findAllOrganisers";

    @Enumerated(EnumType.STRING)
    private UserGroup userGroup;

    public UserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
    }

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(unique = true)
    private String username;

    private String realname;

    @Pattern(regexp = "^[_A-Za-z0-9-\\\\+]+(\\\\.[_A-Za-z0-9-]+)*@"
            + "uni-koblenz.de", message = "Invalid email address.")
    @Column(unique = true)
    private String email;
    private String password;

    @ManyToMany(mappedBy = "organisers")
    private List<Poll> polls;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Poll> getPolls() {
        return polls;
    }

    public void setPolls(List<Poll> polls) {
        this.polls = polls;
    }

    @Override
    public String toString() {
        return "Organiser{"
                + "userGroup=" + userGroup
                + ", id=" + id
                + ", username='" + username + '\''
                + ", realname='" + realname + '\''
                + ", email='" + email + '\''
                + ", password='" + password + '\''
                + ", polls=" + polls
                + '}';
    }
}
