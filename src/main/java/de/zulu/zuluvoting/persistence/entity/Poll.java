package de.zulu.zuluvoting.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NamedQueries({
    @NamedQuery(name = Poll.findAll, query = "SELECT p FROM Poll p"),
    @NamedQuery(name = Poll.findAllPollOwnedByUser, query = "SELECT p FROM Poll p WHERE :user MEMBER OF p.organisers"),
    @NamedQuery(name = Poll.findOne, query = "SELECT p FROM Poll p WHERE p.id = :id")

})
@Entity
public class Poll implements Serializable {

    public static final String findAll = "Poll.findAll";
    public static final String findAllPollOwnedByUser = "Poll.findAllPollOwnedByUser";
    public static final String findOne = "Poll.findOne";

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private int voteCount;

    @Column(unique = true)

    private String title;

    @Column(length = 1000)
    private String description;

    @Enumerated(EnumType.STRING)
    private PollState pollState;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date endDate;

    private boolean participationTracking;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Size(min = 1)
    private List<Item> items;

    @Size(min = 3)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Participant> participants;

    @OneToMany(mappedBy = "poll", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Token> tokens;

    @ManyToMany
    private List<Organiser> organisers;

    public Poll() {
        items = new ArrayList<>();
        participants = new ArrayList<>();
        tokens = new ArrayList<>();
        organisers = new ArrayList<>();
        voteCount = 0;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PollState getState() {
        return pollState;
    }

    public void setState(PollState state) {
        this.pollState = state;
    }

    public Date getStart() {
        return startDate;
    }

    public void setStart(Date start) {
        this.startDate = start;
    }

    public Date getEnd() {
        return endDate;
    }

    public void setEnd(Date end) {
        this.endDate = end;
    }

    public boolean isParticipationTracking() {
        return participationTracking;
    }

    public void setParticipationTracking(boolean participationTracking) {
        this.participationTracking = participationTracking;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }

    public List<Organiser> getOrganisers() {
        return organisers;
    }

    public void setOrganisers(List<Organiser> organisers) {
        this.organisers = organisers;
    }

    public PollState getPollState() {
        return pollState;
    }

    public void setPollState(PollState pollState) {
        this.pollState = pollState;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Poll{"
                + "id=" + id
                + ", title='" + title + '\''
                + ", description='" + description + '\''
                + ", pollState=" + pollState
                + ", startDate=" + startDate
                + ", endDate=" + endDate
                + ", participationTracking=" + participationTracking
                + ", items=" + items
                + ", participants=" + "(...)"
                + ", tokens=" + "(...)"
                + ", organisers=" + organisers
                + '}';
    }

}
