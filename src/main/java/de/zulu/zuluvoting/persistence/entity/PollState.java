package de.zulu.zuluvoting.persistence.entity;

public enum PollState {
    /**
     * The Poll has been created and is currently being prepared by the
     * Organisers.
     */
    PREPARED,
    /**
     * The Poll has been started by the Organisers and is waiting to reach the
     * chosen start-date.
     */
    STARTED,
    /**
     * The Poll is running and votes can be submitted by Participants
     */
    RUNNING,
    /**
     * The Poll has timed out and no further votes are accepted.
     */
    FINISHED
}
