package de.zulu.zuluvoting.persistence.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({
    @NamedQuery(name = Token.findOne, query = "SELECT t FROM Token t WHERE t.id = :id"),
    @NamedQuery(name = Token.findOneByValue, query = "SELECT t FROM Token t WHERE t.value = :value")
})
public class Token implements Serializable {

    public static final String findOne = "Token.findOne";
    public static final String findOneByValue = "Token.findOneByValue";

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "token_value", unique = true)
    private String value;

    /**
     * May be null if participation tracking is disabled
     */
    @OneToOne
    private Participant participant;

    /**
     * May be null if participation tracking is disabled
     */
    @ManyToOne
    private Poll poll;

    private boolean valid;

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Token() {
        valid = true;
    }

    public Token(String value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Token{"
                + "id=" + id
                + ", value='" + value + '\''
                + ", participant=" + participant
                + ", poll=" + poll
                + '}';
    }
}
