package de.zulu.zuluvoting.persistence.entity;

public enum UserGroup {
    USER,
    ADMIN;
}
