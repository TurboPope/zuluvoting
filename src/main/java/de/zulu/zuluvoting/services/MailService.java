package de.zulu.zuluvoting.services;

import java.util.List;

public interface MailService {

    String getBody();

    String getSubject();

    String getTo();

    List<String> poll() throws Exception;

    void send() throws Exception;

    void setBody(String body);

    void setSubject(String subject);

    void setTo(String to);

}
