package de.zulu.zuluvoting.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Named
@SessionScoped
public class MailServiceBean implements Serializable, MailService {

    private static final long serialVersionUID = 1544680932114626710L;

    @Resource(name = "mail/gMail")
    private Session mySession;

    private String to;
    private final String from = "zuluvoting@gmail.com";
    private String subject;
    private String body;

    @Override
    public String getTo() {
        return to;
    }

    @Override
    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public void send() throws Exception {
        Message message = new MimeMessage(mySession);
        message.setFrom(new InternetAddress(from));
        Address toAddress = new InternetAddress(to);
        message.addRecipient(Message.RecipientType.TO, toAddress);
        message.setSubject(subject);
        message.setContent(body, "text/plain");
        Transport.send(message);
    }

    @Override
    public List<String> poll() throws Exception {
        long start = System.currentTimeMillis();
        List<String> list = new ArrayList<String>();

        Store store = null;
        Folder inbox = null;
        try {
            // List all messages in the INBOX
            store = mySession.getStore();
            store.connect();

            inbox = store.getFolder("INBOX");

            inbox.open(Folder.READ_WRITE);

            Message[] messages = inbox.getMessages();
            for (Message m : messages) {
                list.add(m.getSubject());
            }
        } catch (Exception ex) {
            System.err.println("Unknown error occured:" + ex.getMessage());
        } finally {
            try {
                if (null != inbox) {
                    inbox.close(true);
                }
                if (null != store) {
                    store.close();
                }
            } catch (Exception ex) {
                System.err.println("Unable to close IMAP store connection");
            }
        }
        return list;
    }
}
