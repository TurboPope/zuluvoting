package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.persistence.entity.Organiser;
import java.util.List;

public interface OrganiserService {

    void deleteOrganiser(Organiser organiser);

    void createOrganiser(Organiser organiser);

    void updateOrganiser(Organiser organiser);

    List<Organiser> getAllOrgnanisers();
}
