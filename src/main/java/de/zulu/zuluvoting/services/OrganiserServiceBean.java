/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.persistence.entity.Organiser;
import de.zulu.zuluvoting.persistence.entity.Poll;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author dawid
 */
@Stateless
public class OrganiserServiceBean implements OrganiserService {
  @PersistenceContext
    private EntityManager entityManager;
  
    @Override
    @RolesAllowed("ADMIN")
    public void deleteOrganiser(Organiser organiser) {
       Organiser managedOrganiser = entityManager.find(Organiser.class, organiser.getId());
       entityManager.remove(managedOrganiser);
    }

    @Override
    public void createOrganiser(Organiser organiser) {
        entityManager.persist(organiser);
                
    }

    @Override
    public void updateOrganiser(Organiser organiser) {
      entityManager.merge(organiser);
    }

    @Override
    @RolesAllowed("ADMIN")
    public List<Organiser> getAllOrgnanisers() {
       TypedQuery<Organiser> query = entityManager.createNamedQuery(Organiser.findAllOrganisers, Organiser.class);
        List<Organiser> organisers = query.getResultList();
        return organisers;
    }
    
}
