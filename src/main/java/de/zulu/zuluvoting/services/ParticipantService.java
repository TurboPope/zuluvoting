package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.persistence.entity.Participant;

public interface ParticipantService {

    public void updateParticipant(Participant participant);
}
