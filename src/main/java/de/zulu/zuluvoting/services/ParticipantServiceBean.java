package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.persistence.entity.Participant;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ParticipantServiceBean implements ParticipantService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void updateParticipant(Participant participant) {
        entityManager.merge(participant);
    }
}
