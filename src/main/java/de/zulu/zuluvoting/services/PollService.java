package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.persistence.entity.Organiser;
import de.zulu.zuluvoting.persistence.entity.Participant;
import de.zulu.zuluvoting.persistence.entity.Poll;
import java.util.List;

public interface PollService {

    List<Poll> getAllPolls();

    List<Poll> getAllPollsOwnedByCurrentUser();

    void addPoll(Poll poll);

    void deletePoll(Poll poll);

    void updatePoll(Poll poll);

    void removeOrganiserFromPoll(Poll poll, Organiser organiser);

    void removeParticipantFromPoll(Poll poll, Participant participant);

    void addParticipant(Poll poll, Participant participant);

    void updatePollStatus();

    Poll findPollById(long id);
}
