package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.data.PollListProducer;
import de.zulu.zuluvoting.persistence.entity.Organiser;
import de.zulu.zuluvoting.persistence.entity.Participant;
import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.persistence.entity.PollState;
import de.zulu.zuluvoting.persistence.entity.Token;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class PollServiceBean implements PollService {

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    PollListProducer pollListProducer;

    @Inject
    UserService userService;

    @Inject
    TokenService tokenService;

    @Override
    public List<Poll> getAllPolls() {
        TypedQuery<Poll> query = entityManager.createNamedQuery(Poll.findAll, Poll.class);
        List<Poll> polls = query.getResultList();
        return polls;
    }

    @Override
    public List<Poll> getAllPollsOwnedByCurrentUser() {
//        TypedQuery<Poll> query = entityManager.createNamedQuery(Poll.findAllPollOwnedByUser, Poll.class);
//        query.setParameter("user", userService.getOrganiser());
//        List<Poll> polls = query.getResultList();
//        return polls;
        Organiser managedOrganiser = entityManager.find(Organiser.class, userService.getOrganiser().getId());
        return managedOrganiser.getPolls();
    }

    @Override
    public void deletePoll(Poll poll) {
        Poll managedPoll = entityManager.find(Poll.class, poll.getId());
        for (Organiser o : managedPoll.getOrganisers()) {
            //Organiser currentOrganiser = entityManager.find(Organiser.class, o.getId());
            // currentOrganiser.getPolls().remove(managedPoll);
            // entityManager.merge(currentOrganiser);
            o.getPolls().remove(managedPoll);
            entityManager.merge(o);
        }
        entityManager.remove(managedPoll);

    }

    @Override
    public void updatePoll(Poll poll) {
        entityManager.merge(poll);
    }

    @Override
    //removes the rganiser from the list but NOT form db
    public void removeOrganiserFromPoll(Poll poll, Organiser organiser) {
        if (organiser.getId().equals(userService.getOrganiser().getId())) {
            //Organiser shall not be able to remove himself from the organiser list
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "You cannot remove yourself from the organiser list!", null);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } else {
            poll.getOrganisers().remove(organiser);
            updatePoll(poll);
        }
    }

    @Override
    public void removeParticipantFromPoll(Poll poll, Participant participant) {
        poll.getParticipants().remove(participant);
        //updatePoll(poll);
    }

    @Override
    public void addParticipant(Poll poll, Participant participant) {
        poll.getParticipants().add(participant);
        //updatePoll(poll);
    }

    @Override
    public void addPoll(Poll poll) {
        entityManager.persist(poll);

        Organiser currentOrganiser = entityManager.find(Organiser.class, userService.getOrganiser().getId());
        currentOrganiser.getPolls().add(poll);
        entityManager.merge(currentOrganiser);

    }

    private boolean checkHasVoted(Poll poll) {
        for (Participant p : poll.getParticipants()) {
            if (!p.getHasVoted()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void updatePollStatus() {
        TypedQuery<Poll> query = entityManager.createNamedQuery(Poll.findAll, Poll.class);
        List<Poll> polls = query.getResultList();
        Date now = new Date();

        for (Poll poll : polls) {

            if (!(poll.getState().equals(PollState.PREPARED)) && !(poll.getState().equals(PollState.FINISHED))) {

                if (!(poll.getVoteCount() < poll.getParticipants().size())) {
                    poll.setState(PollState.FINISHED);
                    for (Token t : poll.getTokens()) {
                        t.setValid(false);
                    }
                    updatePoll(poll);
                } else //Heute vor Startdatum
                if (now.before(poll.getStart())) {
                    if (!poll.getState().equals(PollState.STARTED)) {
                        poll.setState(PollState.STARTED);
                        updatePoll(poll);
                    }
                }//Heute im Abstimmzeitraum   
                else if (now.after(poll.getStart()) && now.before(poll.getEnd())) {
                    if (!poll.getState().equals(PollState.RUNNING)) {
                        poll.setState(PollState.RUNNING);

                        tokenService.sendMessageToParticipants(poll);

                        updatePoll(poll);
                    }
                } //Heute nach Ablauf
                else if (!poll.getState().equals(PollState.FINISHED)) {
                    poll.setState(PollState.FINISHED);
                    for (Token t : poll.getTokens()) {
                        t.setValid(false);
                    }
                    updatePoll(poll);
                }
            }
        }
    }

    @Override
    public Poll findPollById(long id) {
        return entityManager
                .createNamedQuery(Poll.findOne, Poll.class)
                .setParameter("id", id)
                .getSingleResult();
    }

}
