package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.persistence.entity.Token;

public interface TokenService {

    void sendMessageToParticipants(Poll p);

    public Token findTokenById(Long id);

    public Token findTokenByValue(String value);

    public void updateToken(Token token);
}
