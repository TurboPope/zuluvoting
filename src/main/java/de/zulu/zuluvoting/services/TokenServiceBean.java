package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.persistence.entity.Poll;
import de.zulu.zuluvoting.persistence.entity.Token;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class TokenServiceBean implements TokenService {

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private MailService mailService;

    @Override
    public void sendMessageToParticipants(Poll p) {
        System.out.println("sendMessageToParticipants(Poll p)");
        boolean sendMail = true;
        for (Token t : p.getTokens()) {
            StringBuffer sb = new StringBuffer();
            sb.append("Dear " + t.getParticipant().getEmail() + ",\n");
            sb.append("You have been selected to participate in the poll '" + p.getTitle() + "'\n");
            sb.append("It starts on: " + p.getStartDate() + " and ends on " + p.getEndDate() + "\n");
            sb.append(p.getParticipants().size() + " participants take part in the poll\n");
            sb.append("Please take a vote at: " + "http://localhost:8080/zuluvoting" + "/takePoll" + "?token=" + t.getValue() + "\n");
            sb.append("Or just go to: http://localhost:8080/zuluvoting/takePoll and insert following code: " + t.getValue() + "\n");

            System.out.println(sb.toString());

            /*
            if(sendMail){
                mailService.setBody(sb.toString());
                mailService.setTo("bijak.dawid@gmail.com");
                mailService.setSubject("Poll message test");
                try {
                    mailService.send();
                } catch (Exception ex) {
                    Logger.getLogger(TokenServiceBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                sendMail = false;
            }
             */
        }

    }

    @Override
    public Token findTokenById(Long id) {
        return entityManager
                .createNamedQuery(Token.findOne, Token.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Token findTokenByValue(String value) {
        return entityManager
                .createNamedQuery(Token.findOneByValue, Token.class)
                .setParameter("value", value)
                .getSingleResult();
    }

    @Override
    public void updateToken(Token token) {
        entityManager.merge(token);
    }
}
