package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.persistence.entity.Organiser;

public interface UserService {

    Organiser getOrganiser();

    boolean isAdministrator();

    void logout();
}
