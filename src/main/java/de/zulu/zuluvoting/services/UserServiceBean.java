package de.zulu.zuluvoting.services;

import de.zulu.zuluvoting.persistence.entity.Organiser;
import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SessionScoped
@Named
public class UserServiceBean implements Serializable, UserService {

    @PersistenceContext
    private EntityManager entityManager;

    private Organiser organiser;

    @Override
    public Organiser getOrganiser() {
        if (organiser == null) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            Principal principal = ec.getUserPrincipal();
            if (principal != null) {
                getOrganiserFromDb(principal.getName());
            }

        }
        return organiser;
    }

    private void getOrganiserFromDb(String username) {
        TypedQuery<Organiser> query = entityManager.createNamedQuery(Organiser.findByUsername, Organiser.class);
        query.setParameter("username", username);
        Organiser o = query.getSingleResult();
        if (o == null) {
            organiser = null;
        }
        organiser = o;
    }

    @Override
    public void logout() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        try {
            HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            res.sendRedirect(req.getContextPath());
        } catch (IOException ex) {
            System.err.println("Error at logout!");
        } finally {
            organiser = null;
            FacesContext.getCurrentInstance().responseComplete();
            req.getSession(false).invalidate();
        }
    }

    @Override
    public boolean isAdministrator() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMIN");
    }
}
