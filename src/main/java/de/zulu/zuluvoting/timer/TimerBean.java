package de.zulu.zuluvoting.timer;

import de.zulu.zuluvoting.services.PollService;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class TimerBean {

    @Inject
    PollService pollService;

    @Schedule(hour = "*", minute = "*", second = "0", persistent = false)

    public void myTimer() {
        pollService.updatePollStatus();
    }
}
